`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// ECE369 - Computer Architecture
// 
// Module - ALU32Bit_tb.v
// Description - Test the 'ALU32Bit.v' module.
////////////////////////////////////////////////////////////////////////////////

module ALU32Bit_tb(); 

	reg [4:0] ALUControl;   // control bits for ALU operation
	reg [31:0] A, B;	        // inputs

	wire [31:0] ALUResult;	// answer
	wire Zero;	        // Zero=1 if ALUResult == 0

    ALU32Bit u0(
        .ALUControl(ALUControl), 
        .A(A), 
        .B(B), 
        .ALUResult(ALUResult), 
        .Zero(Zero)
    );

	initial begin
	
    /* Please fill in the implementation here... */
    //Test: ADD
    ALUControl <= 5'd0;
    A <= 32'd10;
    B <= 32'd15;
    //ALUResult should be 25
    
    #5
	
	//Test: SUB
    ALUControl <= 5'd1;
    A <= 32'd15;
    B <= 32'd10;
    //ALUResult should be 5
    
    #5
    
    //Test: MUL
    ALUControl <= 5'd2;
    A <= 32'd15;
    B <= 32'd10;
    //ALUResult should be 150
    
    #5
        
    //Test: MULT
    ALUControl <= 5'd3;
    A <= 32'd5;
    B <= 32'd10;
    //HI = 0, low = 50
    
    #5
    
    //Test: MADD
    ALUControl <= 5'd4;
    A <= 32'd10;
    B <= 32'd20;
    //ALUResult doesn't matter. 
    //HI <= 00000000000000000000000000000000
    //LO <= 00000000000000000000000011001000
    
    #5
    
    //Test: SADD
    ALUControl <= 5'd5;
    A <= 32'd10;
    B <= 32'd20;
    // Both HI and LO should become zero
    
    #5
    
    //Test: MVTH
    ALUControl <= 5'd6;
    A <= 32'd900;
    //HI <= 900
    
    #5
    
    //Test: MVTL
    ALUControl <= 5'd7;
    A <= 32'd800;
    //LO should be 800
    
    #5
    
    //Test: MVFH
    ALUControl <= 5'd8;
    //ALUResult should be 900
    
    #5
    
    //Test: MVFL
    ALUControl <= 5'd9;
    //ALUResult should be 800
    
    #5
    
    //Test: less than
	ALUControl <= 5'd10;
	A <= 32'd10;
	B <= 32'd9;
	//ALUResult should be 0, zero should be 1
	
	#5
	
	//Test: Less than equal to
	ALUControl <= 5'd11;
	A <= 32'd10;
	B <= 32'd10;
	//ALUResult should be 1, zero should be 0
	
	#5
	
	//Test: equal
	ALUControl <= 5'd12;
	A <= 32'd10;
	B <= 32'd10;
	//ALUResult should be 1, zero should be 0
	
	#5
	
	//Test: not equal
	ALUControl <= 5'd13;
	A <= 32'd10;
	B <= 32'd10;
	//ALUResult should be 0, zero should be 1
	
	#5
	
	//Test: greater than
	ALUControl <= 5'd14;
	A <= 32'd12;
	B <= 32'd11;
	//ALUResult should be 1, zero should be 0
	
	#5
	
	//Test: greater than equal to
	ALUControl <= 5'd15;
	A <= 32'd13;
	B <= 32'd13;
	//ALUResult should be 1, zero should be 0
	
	#5
	
	//Test: and
	ALUControl <= 5'd16;
	A <= 32'd1;
	B <= 32'd0;
	//ALUResult should be 0, zero should be 1
	
	#5
        
    //Test: or
    ALUControl <= 5'd17;
    A <= 32'd1;
    B <= 32'd0;
    //ALUResult should be 1, zero should be 0
    
    #5
        
    //Test: nor
    ALUControl <= 5'd18;
    A <= 32'd0;
    B <= 32'd0;
    //ALUResult should be 1, zero should be 0
    
    #5
        
    //Test: xor
    ALUControl <= 5'd19;
    A <= 32'd1;
    B <= 32'd0;
    //ALUResult should be 1, zero should be 0
    
    #5
	
	//Test: sign extend half word
	ALUControl <= 5'd20;
	A <= 32'd5;
	//ALUResult should have leading zeros
	
	#5
	
	//Test: sll
	ALUControl <= 5'd21;
	A <= 32'd2;
	B <= 32'd2;
	//ALUResult sould be 8
	
	
	#5
	
	//Test: srl
	ALUControl <= 5'd22;
	A <= 32'd8;
	B <= 32'd2;
	//ALUResult should be 2
	
	#5
	
	//Test: MOVN
	ALUControl <= 5'd23;
	A <= 32'd100;
	B <= 32'd1;
	//ALUResult should be 100
	
	#5
	
	//Test: MOVZ
	ALUControl <= 5'd24;
	A <= 32'd200;
	B <= 32'd0;
	//ALUReuslt should be 200
	
	#5
	
	//Test: ROTR
	ALUControl <= 5'd25; //check
	A <= 32'd2;
	B <= 32'b00000000000000000000000000000011;
	//ALUResult should be 11000000000000000000000000000000
	
	#5
	
	//Test: sra        //check
	ALUControl <= 5'd26;
	A <= 32'd2;
	B <= 32'b10000000111111111111111111111111;
	//ALUResult should have leading 1s
	
	#5
	
	//Test: sign extend byte
	ALUControl <= 5'd27;
	A <= 32'd00000000000000000000000010000000;
	//ALUResult should have leading 1s
	
	#5
	
	//Test: LUI
	ALUControl <= 5'd28;
	B <= 32'd1;
	//ALUResult should be whatever the hell 1 is shift 16 times
	end

endmodule

