`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 10/03/2018 05:33:01 PM
// Design Name: 
// Module Name: ShiftLeft
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module ShiftLeft(SignExtend,ShiftOut);
    input [31:0] SignExtend;
    output reg [31:0] ShiftOut;
    
    always @(SignExtend)begin
        ShiftOut <= SignExtend << 2;
    end
endmodule
