`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// ECE369 - Computer Architecture
// 
// Module - ALU32Bit.v
// Description - 32-Bit wide arithmetic logic unit (ALU).
//
// INPUTS:-
// ALUControl: N-Bit input control bits to select an ALU operation.
// A: 32-Bit input port A.
// B: 32-Bit input port B.
//
// OUTPUTS:-
// ALUResult: 32-Bit ALU result output.
// ZERO: 1-Bit output flag. 
//
// FUNCTIONALITY:-
// Design a 32-Bit ALU, so that it supports all arithmetic operations 
// needed by the MIPS instructions given in Labs5-8.docx document. 
//   The 'ALUResult' will output the corresponding result of the operation 
//   based on the 32-Bit inputs, 'A', and 'B'. 
//   The 'Zero' flag is high when 'ALUResult' is '0'. 
//   The 'ALUControl' signal should determine the function of the ALU 
//   You need to determine the bitwidth of the ALUControl signal based on the number of 
//   operations needed to support. 
////////////////////////////////////////////////////////////////////////////////

module ALU32Bit(Clk,ALUControl, A, B, PcCounter ,ALUResult, Zero, ChangeRegWrite,HI_in,LO_in,HI_out,LO_out);

    input Clk;
	input [5:0] ALUControl;                    // control bits for ALU operation
    input signed [31:0] HI_in, LO_in;
	input signed [31:0] A, B, PcCounter ;	               // inputs
	reg [31:0 ]A_u, B_u;

    reg signed [63:0] temp;
    reg [63:0] temp_u;

	output reg signed [31:0] ALUResult, HI_out,LO_out;	       // answer
	output reg ChangeRegWrite;                 //If RegWrite Signal needs to be disabled
	output reg Zero;	                       // Zero=1 if ALUResult == 0
	
	initial begin
        HI_out <= 32'd0;
        LO_out <= 32'd0;
        temp <= 64'd0;
        ALUResult <= 32'd0;
        Zero <= 1'd0;    
    end
 
    /* Please fill in the implementation here... */
    always @ (A,B,ALUControl) begin
        //Zero <= 0;
        ChangeRegWrite <= 0;
        case (ALUControl)
            6'd0: ALUResult <= A+B;                                 //Add
            6'd1: ALUResult <= A-B;                                 //Sub
            6'd2: ALUResult <= A*B;                                 //Mul
            6'd3: begin                                             //Mult
                temp = A*B;
                HI_out <= temp[63:32];
                LO_out <= temp[31:0];
            end
            6'd4: begin                                             //Multu
                A_u = A;
                B_u = B;                                       
                temp = A_u*B_u;
                HI_out <= temp[63:32];
                LO_out <= temp[31:0];
            end
            6'd5: begin                                             //Multiply and Add Word to Hi, Lo. ALUResult should not be used.
                temp =  A*B;
                temp = {HI_in,LO_in} + temp;
                HI_out <= temp[63:32];
                LO_out <= temp[31:0];
            end
            6'd6: begin                                             //Multiply and Sub Word to Hi, Lo. ALUResult should not be used.
                temp =  A*B;
                temp = {HI_in,LO_in} - temp;
                HI_out <= temp[63:32];
                LO_out <= temp[31:0];
            end
            6'd7: begin                                             //Move to high register. Check that A is rs
                HI_out <= A; 
            end                                     
            6'd8: begin                                             //Move to low reigster. Check that A is rs
                LO_out <= A;
            end                                      
            6'd9: ALUResult <= HI_in;                                  //Move from HI register.    *Might need to be move to clock.
            6'd10: ALUResult <= LO_in;                                 //Move from LO register.    *Might need to be move to clock.
            6'd11: ALUResult <= B << 16;                            //LUI
            6'd12: begin                                            //BGEZ
                if (A >= 0 ) begin
                    ALUResult <= 0;
                end
                else begin
                    ALUResult <= 1;
                end
            end
            6'd13: begin                                            //BNE
                if (A != B) begin
                    ALUResult <= 0;
                end
                else begin
                    ALUResult <= 1;
                end
            end
            6'd14: begin                                            //BGTZ
                if (A > 0) begin
                    ALUResult <= 0;
                end
                else begin
                    ALUResult <= 1;
                end
            end
            6'd15: begin                                            //BLEZ
                if (A <= 0) begin
                    ALUResult <= 0;
                end
                else begin
                    ALUResult <= 1;
                end
            end
            6'd16: begin                                            //BLTZ
                if (A < 0) begin
                    ALUResult <= 0;
                end
                else begin
                    ALUResult <= 1;
                end
            end
            6'd17: ALUResult <= A & B;                              //And
            6'd18: ALUResult <= A | B;                              //Or
            6'd19: ALUResult <= ~(A | B);                           //Nor
            6'd20: ALUResult <= A ^ B;                              //Xor
            6'd21: ALUResult <= {{16{B[15]}},B[15:0]};              //Sign extend half word
            6'd22: ALUResult <= A << B[10:6];                       //Shift left logical
            6'd23: ALUResult <= A >> B[10:6];                       //Shift right logical
            6'd24: ALUResult <= B << A;                             //Shift left logical variable
            6'd25: ALUResult <= B >> A;                             //Shift right logical variable
            6'd26: begin                                            //Set on less than
                if (A < B) begin                                   
                    ALUResult <= 1;
                end
                else begin
                    ALUResult <= 0;
                end
            end
            6'd27: begin
                if (B != 0) begin                                   //MOVN. If rt(B) != 0, then result is rs
                    ALUResult <= A;
                end
                else begin
                    ChangeRegWrite <= 1;
                end
            end
            6'd28: begin                                            //MOVZ.If rt(B) = 0, then result is rs.
                if (B == 0) begin
                    ALUResult <= A;
                end
                else begin
                    ChangeRegWrite <= 1;
                end
            end
            6'd29: ALUResult <= (B >> A) | (B << (32-A[4:0]));           //ROTRV
            6'd30: ALUResult <= (A >> B[10:6]) | (A << (32-B[10:6]));//ROTR    
            6'd31: ALUResult <= A >>> B[10:6];                      //SRA
            6'd32: ALUResult <= B >>> A;                            //SRAV
            6'd33: ALUResult <= {{24{A[7]}},A[7:0]};                //SEB
            6'd34: begin                                            //SLTU || SLTIU
                A_u = A;
                B_u = B;
                if (A_u < B_u) begin
                    ALUResult <= 1;
                end
                else begin
                    ALUResult <= 0;
                end
            end
            6'd35: begin
                ALUResult  <= PcCounter  + 4;    
                Zero <= 1; 
            end 
            6'd36: ALUResult <= 0;
        endcase
        Zero = (ALUResult ==0 || ALUControl == 6'd35)?1:0;      //Change if jump alu signal becomes something besides 35
    end
    
endmodule

