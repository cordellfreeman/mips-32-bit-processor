`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 10/03/2018 05:37:43 PM
// Design Name: 
// Module Name: ProgramCounter
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module ProgramCounter(Address, PCResult, Reset, Clk,debug_reg_PCOut);

	input [31:0] Address;
	input Reset, Clk;

	(*mark_debug = "true" *)output reg [31:0] PCResult;
	(*mark_debug = "true" *) output wire [31:0] debug_reg_PCOut;
	assign debug_reg_PCOut = PCResult;

    /* Please fill in the implementation here... */
    always @(posedge Clk) begin
        if(Reset)
            PCResult <= 0;
        else
            PCResult <= Address;
            
    end
endmodule
