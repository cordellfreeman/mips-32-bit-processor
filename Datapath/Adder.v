`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 10/03/2018 05:26:32 PM
// Design Name: 
// Module Name: Adder
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////

module Adder(PCAdderResult,ShiftResult,Result);
    input [31:0] PCAdderResult, ShiftResult;
    output reg [31:0] Result;
    always @(PCAdderResult,ShiftResult) begin
        Result <= PCAdderResult + ShiftResult;
    end
endmodule

