`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 10/03/2018 05:37:18 PM
// Design Name: 
// Module Name: PCAdder
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module PCAdder(PCResult, PCAddResult);

    input [31:0] PCResult;

    output reg [31:0] PCAddResult;

    /* Please fill in the implementation here... */
    always @(PCResult) begin
        PCAddResult <= PCResult + 4;
    end
endmodule

