`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 10/03/2018 01:55:57 PM
// Design Name: 
// Module Name: MEMWBRegister
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module MEMWBRegister(Clk, MemToRegSignal,RegWriteSignal,DataFromMem,ALUResult, RegDstAddress,  
                     MemToRegSignalOut,RegWriteSignalOut,DataFromMemOut,ALUResultOut,RegDstAddressOut,debug_regWrite);
    input Clk;                 
                     
    input MemToRegSignal,RegWriteSignal;
    output reg MemToRegSignalOut,RegWriteSignalOut;
    
    input [31:0] DataFromMem, ALUResult;
    output reg [31:0] DataFromMemOut, ALUResultOut;
    
    input [4:0] RegDstAddress;
    output reg [4:0] RegDstAddressOut;
    
    (* mark_debug = "true" *)  output wire debug_regWrite;
             assign debug_regWrite = RegWriteSignalOut;
    
    always @(posedge Clk)begin
        MemToRegSignalOut <= MemToRegSignal;
        RegWriteSignalOut <= RegWriteSignal;
        
        DataFromMemOut <= DataFromMem;
        ALUResultOut <= ALUResult;
        
        RegDstAddressOut <= RegDstAddress;
    end
    
    
endmodule
