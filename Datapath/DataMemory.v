`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// ECE369 - Computer Architecture
// 
// Module - data_memory.v
// Description - 32-Bit wide data memory.
//
// INPUTS:-
// Address: 32-Bit address input port.
// WriteData: 32-Bit input port.
// Clk: 1-Bit Input clock signal.
// MemWrite: 1-Bit control signal for memory write.
// MemRead: 1-Bit control signal for memory read.
//
// OUTPUTS:-
// ReadData: 32-Bit registered output port.
//
// FUNCTIONALITY:-
// Design the above memory similar to the 'RegisterFile' model in the previous 
// assignment.  Create a 1K memory, for which we need 10 bits.  In order to 
// implement byte addressing, we will use bits Address[11:2] to index the 
// memory location. The 'WriteData' value is written into the address 
// corresponding to Address[11:2] in the positive clock edge if 'MemWrite' 
// signal is 1. 'ReadData' is the value of memory location Address[11:2] if 
// 'MemRead' is 1, otherwise, it is 0x00000000. The reading of memory is not 
// clocked.
//
// you need to declare a 2d array. in this case we need an array of 1024 (1K)  
// 32-bit elements for the memory.   
// for example,  to declare an array of 256 32-bit elements, declaration is: reg[31:0] memory[0:255]
// if i continue with the same declaration, we need 8 bits to index to one of 256 elements. 
// however , address port for the data memory is 32 bits. from those 32 bits, least significant 2 
// bits help us index to one of the 4 bytes within a single word. therefore we only need bits [9-2] 
// of the "Address" input to index any of the 256 words. 
////////////////////////////////////////////////////////////////////////////////

module DataMemory(Address, WriteData, Clk, MemWrite, MemRead, ReadData); 

    input [31:0] Address; 	// Input Address 
    input [31:0] WriteData; // Data that needs to be written into the address 
    input Clk;
    input [1:0] MemWrite; 		// Control signal for memory write 
    input [1:0] MemRead; 			// Control signal for memory read 

    output reg[31:0] ReadData; // Contents of memory location at Address

    /* Please fill in the implementation here */
    reg [31:0] Memory[0:1023];
    integer i;
    
    initial begin
        Memory[0] <= 32'h00000000;
        Memory[1] <= 32'h00000001;
        Memory[2] <= 32'h00000002;
        Memory[3] <= 32'h00000003;
        Memory[4] <= 32'h00000004;
        Memory[5] <= -32'h00000001;
        for (i = 6; i < 1024; i = i + 1) begin
            Memory[i] = 32'd0;
        end
    end
    
    always @ * begin
        if(MemRead == 2'd1) begin
            ReadData <= Memory[Address[11:2]];
        end
        else if(MemRead == 2'd2) begin
            ReadData <= {{16{Memory[Address[11:2]][15]}},Memory[Address[11:2]][15:0]};
        end
        else if(MemRead == 2'd3) begin
            ReadData <= {{24{Memory[Address[11:2]][7]}},Memory[Address[11:2]][7:0]};
        end
        else begin
            ReadData <= 32'h0;
        end
    end
    
    always @ (posedge Clk) begin
        if(MemWrite == 2'd1) begin
            Memory[Address[11:2]] <= WriteData;
        end
        else if (MemWrite == 2'd2) begin
            Memory[Address[11:2]] <= {{16{WriteData[15]}},WriteData[15:0]};
        end
        else if (MemWrite == 2'd3) begin
            Memory[Address[11:2]] <= {{24{WriteData[7]}},WriteData[7:0]};   
        end
    end
endmodule
