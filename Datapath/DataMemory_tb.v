`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// ECE369 - Computer Architecture
// 
// Module - DataMemory_tb.v
// Description - Test the 'DataMemory.v' module.
////////////////////////////////////////////////////////////////////////////////

module DataMemory_tb(); 

    reg     [31:0]  Address;
    reg     [31:0]  WriteData;
    reg             Clk;
    reg             MemWrite;
    reg             MemRead;

    wire [31:0] ReadData;

    DataMemory u0(
        .Address(Address), 
        .WriteData(WriteData), 
        .Clk(Clk), 
        .MemWrite(MemWrite), 
        .MemRead(MemRead), 
        .ReadData(ReadData)
    ); 

	initial begin
		Clk <= 1'b0;
		forever #10 Clk <= ~Clk;
	end

	initial begin
	
    /* Please fill in the implementation here... */
   MemRead <= 1;
   MemWrite <= 0;
   Address <= 32'b00000000000000000000000000000100;
   WriteData <= 0;
   #15;
   MemRead <= 1;
   MemWrite <= 0;
   Address <= 32'b00000000000000000000000000011100;
   WriteData <= 0;
   #15;
   MemRead <= 0;
   MemWrite <= 1;
   Address <= 32'b00000000000000000000000000010000;
   WriteData <=32'd10;
	
	end

endmodule

