`timescale 1ns / 1ps

module IDEXRegister(Clk,MemToRegSignal,RegWriteSignal,MemWriteSignal,MemReadSignal, BranchSignal,ALUSrc1Signal,ALUSrc2Signal,ALUSrc3Signal, RegDstSignal, ALUOp,PCAdderResult, ReadData1, ReadData2, SignExtendResult,
       Instruction20_16,Instruction15_11,writeHILO,ZeroExtend,MemWriteSignalOut,MemReadSignalOut, BranchSignalOut,ALUSrc1SignalOut,ALUSrc2SignalOut,ALUSrc3SignalOut ,RegDstSignalOut,ALUOpOut,
       PCAdderResultOut, ReadData1Out, ReadData2Out, SignExtendResultOut,Instruction20_16Out,Instruction15_11Out,MemToRegSignalOut,RegWriteSignalOut,writeHILOOut,ZeroExtendOut,branchOrJump,branchOrJumpOut,Instruction25_0_In,Instruction25_0_Out);
       
    input Clk;
    
    input MemToRegSignal,RegWriteSignal;
    output reg MemToRegSignalOut,RegWriteSignalOut;
    
    input BranchSignal; //MEM Signals
    input [1:0] MemWriteSignal,MemReadSignal;
    output reg BranchSignalOut; //MEM Output Signals
    output reg [1:0] MemWriteSignalOut,MemReadSignalOut;
    
    input ALUSrc1Signal, ALUSrc2Signal,ALUSrc3Signal, writeHILO; //EX Signals
    input [1:0] branchOrJump, RegDstSignal;
    input [25:0] Instruction25_0_In;
    output reg ALUSrc1SignalOut,ALUSrc2SignalOut,ALUSrc3SignalOut, writeHILOOut;//EX output Signals
    output reg [1:0] branchOrJumpOut, RegDstSignalOut; 
    input [5:0] ALUOp;                //EX Signals
    output reg [5:0] ALUOpOut;              //EX output Signals
    output reg [25:0] Instruction25_0_Out;
    
    input [31:0] PCAdderResult, ReadData1, ReadData2, SignExtendResult,ZeroExtend; //ZeroExtendResult;
    output reg [31:0] PCAdderResultOut, ReadData1Out, ReadData2Out, SignExtendResultOut,ZeroExtendOut; //ZeroExtendResultOut;
    input [4:0] Instruction20_16,Instruction15_11;
    output reg [4:0] Instruction20_16Out,Instruction15_11Out;
    
    

    
    
    always @ (posedge Clk) begin
        //WB Signals
        MemToRegSignalOut <= MemToRegSignal;
        RegWriteSignalOut <= RegWriteSignal;
        
        //MEM Signals
        MemWriteSignalOut <= MemWriteSignal;
        MemReadSignalOut <= MemReadSignal;
        BranchSignalOut <= BranchSignal;
        
        //EX Signals
        ALUSrc1SignalOut <= ALUSrc1Signal;
        ALUSrc2SignalOut <= ALUSrc2Signal;
        ALUSrc3SignalOut <= ALUSrc3Signal;
        ZeroExtendOut <= ZeroExtend;
        writeHILOOut <= writeHILO;
        RegDstSignalOut <= RegDstSignal;
        ALUOpOut <= ALUOp;
        branchOrJumpOut <= branchOrJump;
        Instruction25_0_Out <= Instruction25_0_In;
        
        PCAdderResultOut <= PCAdderResult;
        ReadData1Out <= ReadData1;
        ReadData2Out <= ReadData2;
        SignExtendResultOut <= SignExtendResult;
        //ZeroExtendResultOut <= ZeroExtendResult;
        
        Instruction20_16Out <= Instruction20_16;
        Instruction15_11Out <= Instruction15_11;
    end
endmodule