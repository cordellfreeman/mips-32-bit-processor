`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// ECE369 - Computer Architecture
// 
// Module - RegisterFile.v
// Description - Test the register_file
// Suggested test case - First write arbitrary values into 
// the saved and temporary registers (i.e., register 8 through 25). Then, 2-by-2, 
// read values from these registers.
////////////////////////////////////////////////////////////////////////////////


module RegisterFile_tb();

	reg [4:0] ReadRegister1;
	reg [4:0] ReadRegister2;
	reg	[4:0] WriteRegister;
	reg [31:0] WriteData;
	reg RegWrite;
	reg Clk;

	wire [31:0] ReadData1;
	wire [31:0] ReadData2;


	RegisterFile u0(
		.ReadRegister1(ReadRegister1), 
		.ReadRegister2(ReadRegister2), 
		.WriteRegister(WriteRegister), 
		.WriteData(WriteData), 
		.RegWrite(RegWrite), 
		.Clk(Clk), 
		.ReadData1(ReadData1), 
		.ReadData2(ReadData2)
	);

	initial begin
		Clk <= 1'b1;
		forever #10 Clk <= ~Clk;
	end

	initial begin
	
    /* Please fill in the implementation here... */
    ReadRegister1 <= 5'b00000;
	ReadRegister2 <= 5'b00001;
	RegWrite <= 0;
	WriteRegister <= 5'b00001;
	WriteData <= 32'd0;
	
	#15
	
	ReadRegister1 <= 5'b00000;
    ReadRegister2 <= 5'b00001;
    RegWrite <= 1;
    WriteRegister <= 5'b00001;
    WriteData <= 32'd1;
    
    #15
    
    ReadRegister1 <= 5'b11111;
    ReadRegister2 <= 5'b00111;
    RegWrite <= 1;
    WriteRegister <= 5'b00001;
    WriteData <= 32'd2;
	
	end

endmodule
