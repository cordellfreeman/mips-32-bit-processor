`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 10/08/2018 12:39:49 PM
// Design Name: 
// Module Name: Controller
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module Controller(Instruction,MemToRegSignal,RegWriteSignal,MemWriteSignal,MemReadSignal, BranchSignal,
                  ALUSrc1Signal, ALUSrc2Signal, ALUSrc3Signal ,RegDstSignal, ALUOp,writeHILO, branchOrJump);
    input[31:0] Instruction;

    output reg MemToRegSignal,RegWriteSignal,BranchSignal; 
    output reg [1:0] MemWriteSignal,MemReadSignal;
    output reg ALUSrc1Signal,ALUSrc2Signal,ALUSrc3Signal,writeHILO;
    output reg [1:0] branchOrJump,RegDstSignal; //for branchOrJump 0 is branch, 1 is j/jal, 2 is jr
    output reg [5:0] ALUOp;
    
    always @ (Instruction)begin
        //Start of arithmetic instructions
        ALUSrc3Signal <= 0;
        writeHILO <= 0;
        branchOrJump <= 0;
        if (Instruction == 32'd0) begin
            RegWriteSignal <= 0;
            //ALUSrc1Signal <= x;
            //ALUSrc2Signal <= x; 
            //RegDstSignal <= x; 
            MemReadSignal <= 0; 
            MemWriteSignal <= 0; 
            BranchSignal <= 0; 
            //MemToRegSignal <= x; 
            //ALUOp <= x; 
        end
        if(Instruction[31:26] == 6'b000000 && Instruction [5:0] == 6'b100000) begin //Add
            RegWriteSignal <= 1;
            ALUSrc1Signal <= 0;
            ALUSrc2Signal <= 0; 
            RegDstSignal <= 1; 
            MemReadSignal <= 0; 
            MemWriteSignal <= 0; 
            BranchSignal <= 0; 
            MemToRegSignal <= 1; 
            ALUOp <= 6'd0;  
        end
        else if (Instruction[31:26] == 6'b001001 )begin //Addiu
            RegWriteSignal <= 1;
            ALUSrc1Signal <= 1;
            ALUSrc2Signal <= 0; 
            RegDstSignal <= 0; 
            MemReadSignal <= 0; 
            MemWriteSignal <= 0; 
            BranchSignal <= 0; 
            MemToRegSignal <= 1; 
            ALUOp <= 6'd0;
        end
        else if (Instruction[31:26] == 6'b000000 && Instruction[5:0] == 6'b100001)begin //Addu
            RegWriteSignal <= 1;
            ALUSrc1Signal <= 0;
            ALUSrc2Signal <= 0; 
            RegDstSignal <= 1; 
            MemReadSignal <= 0; 
            MemWriteSignal <= 0; 
            BranchSignal <= 0; 
            MemToRegSignal <= 1; 
            ALUOp <= 6'd0;
        end
        else if (Instruction[31:26] == 6'b001000)begin //Addi
            RegWriteSignal <= 1;
            ALUSrc1Signal <= 1; 
            ALUSrc2Signal <= 0;
            RegDstSignal <= 0; 
            MemReadSignal <= 0; 
            MemWriteSignal <= 0; 
            BranchSignal <= 0; 
            MemToRegSignal <= 1 ; 
            ALUOp <= 6'd0;
        end
        else if (Instruction[31:26] == 6'b000000 && Instruction[5:0] == 6'b100010)begin //Sub
            RegWriteSignal <= 1;
            ALUSrc1Signal <= 0;
            ALUSrc2Signal <= 0; 
            RegDstSignal <= 0; 
            MemReadSignal <= 0; 
            MemWriteSignal <= 0; 
            BranchSignal <= 0; 
            MemToRegSignal <= 1; 
            ALUOp <= 6'd1;
        end
        else if (Instruction[31:26] == 6'b011100 && Instruction[5:0] == 6'b000010)begin //Mul
            RegWriteSignal <= 1;
            ALUSrc1Signal <= 0;
            ALUSrc2Signal <= 0; 
            RegDstSignal <= 1; 
            MemReadSignal <= 0; 
            MemWriteSignal <= 0; 
            BranchSignal <= 0; 
            MemToRegSignal <= 1; 
            ALUOp <= 6'd2;
        end
        else if (Instruction[31:26] == 6'b000000 && Instruction[5:0] == 6'b011000)begin //Mult
            RegWriteSignal <= 1;
            ALUSrc1Signal <= 0; 
            ALUSrc2Signal <= 0;
            //RegDstSignal <= X; 
            MemReadSignal <= 0; 
            MemWriteSignal <= 0; 
            BranchSignal <= 0; 
            //MemToRegSignal <= X; 
            writeHILO <= 1;
            ALUOp <= 6'd3;
        end
        else if (Instruction[31:26] == 6'b000000 && Instruction[5:0] == 6'b011001)begin //Multu
            RegWriteSignal <= 1;
            ALUSrc1Signal <= 0;
            ALUSrc2Signal <= 0; 
            //RegDstSignal <= X; 
            MemReadSignal <= 0; 
            MemWriteSignal <= 0; 
            BranchSignal <= 0; 
            //MemToRegSignal <= X; 
            writeHILO <= 1;
            ALUOp <= 6'd4;
        end
        else if (Instruction[31:26] == 6'b011100 && Instruction[5:0] == 6'b000000)begin //Madd
            RegWriteSignal <= 1;
            ALUSrc1Signal <= 0; 
            ALUSrc2Signal <= 0;
            //RegDstSignal <= X; 
            MemReadSignal <= 0; 
            MemWriteSignal <= 0; 
            BranchSignal <= 0; 
            //MemToRegSignal <= X; 
            writeHILO <= 1;
            ALUOp <= 6'd5;
        end
        else if (Instruction[31:26] == 6'b011100 && Instruction[5:0] == 6'b000100)begin //Msub
            RegWriteSignal <= 1;
            ALUSrc1Signal <= 0; 
            ALUSrc2Signal <= 0;
            //RegDstSignal <= X; 
            MemReadSignal <= 0; 
            MemWriteSignal <= 0; 
            BranchSignal <= 0; 
            //MemToRegSignal <= X; 
            writeHILO <= 1;
            ALUOp <= 6'd6;
        end
        //End of arithmetic instructions
        
        //Start of data instructions
        else if (Instruction[31:26] == 6'b100011)begin //Lw
            RegWriteSignal <= 1;
            ALUSrc1Signal <= 1; 
            ALUSrc2Signal <= 0;
            ALUSrc3Signal <= 0;
            RegDstSignal <= 0; 
            MemReadSignal <= 1; 
            MemWriteSignal <= 0; 
            BranchSignal <= 0; 
            MemToRegSignal <= 0; 
            ALUOp <= 6'd0;
        end
        else if (Instruction[31:26] == 6'b101011)begin //Sw
            RegWriteSignal <= 0;
            ALUSrc1Signal <= 1; 
            ALUSrc2Signal <= 0;
            ALUSrc3Signal <= 0;
            //RegDstSignal <= X; 
            MemReadSignal <= 0; 
            MemWriteSignal <= 1; 
            BranchSignal <= 0; 
            //MemToRegSignal <= X; 
            ALUOp <= 6'd0;
        end
        else if (Instruction[31:26] == 6'b101000)begin //Sb
            RegWriteSignal <= 0;
            ALUSrc1Signal <= 1; 
            ALUSrc2Signal <= 0;
            ALUSrc3Signal <= 0;
            //RegDstSignal <= X; 
            MemReadSignal <= 0; 
            MemWriteSignal <= 3; 
            BranchSignal <= 0; 
            //MemToRegSignal <= X; 
            ALUOp <= 6'd0;
        end
        else if (Instruction[31:26] == 6'b100001)begin //lh
            RegWriteSignal <= 1;
            ALUSrc1Signal <= 1; 
            ALUSrc2Signal <= 0;
            ALUSrc3Signal <= 0;
            RegDstSignal <= 0; 
            MemReadSignal <= 2; 
            MemWriteSignal <= 0; 
            BranchSignal <= 0; 
            MemToRegSignal <= 0; 
            ALUOp <= 6'd0;
        end
        else if (Instruction[31:26] == 6'b100000)begin //lb
            RegWriteSignal <= 1;
            ALUSrc1Signal <= 1; 
            ALUSrc2Signal <= 0;
            ALUSrc3Signal <= 0;
            RegDstSignal <= 0; 
            MemReadSignal <= 3; 
            MemWriteSignal <= 0; 
            BranchSignal <= 0; 
            MemToRegSignal <= 0; 
            ALUOp <= 6'd0;
        end
        else if (Instruction[31:26] == 6'b101001)begin //Sh
            RegWriteSignal <= 0;
            ALUSrc1Signal <= 1; 
            ALUSrc2Signal <= 0;
            ALUSrc3Signal <= 0;
            //RegDstSignal <= X; 
            MemReadSignal <= 0; 
            MemWriteSignal <= 2; 
            BranchSignal <= 0; 
            //MemToRegSignal <= X; 
            ALUOp <= 6'd0;
        end
        else if (Instruction[31:26] == 6'b000000 && Instruction[5:0] == 6'b010001)begin //Mthi
            RegWriteSignal <= 0;
            //ALUSrc1Signal <= X;
            ALUSrc2Signal <= 0; 
            //RegDstSignal <= X; 
            MemReadSignal <= 0; 
            MemWriteSignal <= 0; 
            BranchSignal <= 0; 
            //MemToRegSignal <= X; 
            writeHILO <= 1;
            ALUOp <= 6'd7;
        end
        else if (Instruction[31:26] == 6'b000000 && Instruction[5:0] == 6'b010011)begin //Mtlo
            RegWriteSignal <= 0;
            //ALUSrc1Signal <= X; 
            ALUSrc2Signal <= 0;
            //RegDstSignal <= X; 
            MemReadSignal <= 0; 
            MemWriteSignal <= 0; 
            BranchSignal <= 0; 
            //MemToRegSignal <= X; 
            writeHILO <= 1;
            ALUOp <= 6'd8;
        end
        else if (Instruction[31:26] == 6'b000000 && Instruction[5:0] == 6'b010000)begin //Mfhi
            RegWriteSignal <= 1;
            //ALUSrc1Signal <= X;
            //ALUSrc2Signal <= X; 
            RegDstSignal <= 1; 
            MemReadSignal <= 0; 
            MemWriteSignal <= 0; 
            BranchSignal <= 0; 
            MemToRegSignal <= 1; 
            ALUOp <= 6'd9;
        end
        else if (Instruction[31:26] == 6'b000000 && Instruction[5:0] == 6'b010010)begin //Mflo
            RegWriteSignal <= 1;
            //ALUSrc1Signal <= X;
            //ALUSrc2Signal <= X; 
            RegDstSignal <= 1; 
            MemReadSignal <= 0; 
            MemWriteSignal <= 0; 
            BranchSignal <= 0; 
            MemToRegSignal <= 1; 
            ALUOp <= 6'd10;
        end
        else if (Instruction[31:26] == 6'b001111)begin //Lui
            RegWriteSignal <= 1;
            ALUSrc1Signal <= 1;
            //ALUSrc2Signal <= 0; 
            ALUSrc3Signal <= 0;
            RegDstSignal <= 0; 
            MemReadSignal <= 0; 
            MemWriteSignal <= 0; 
            BranchSignal <= 0; 
            MemToRegSignal <= 1; 
            ALUOp <= 6'd11;
        end
        //End of data instructions
        
        //Start of branch instructions
        else if (Instruction[31:26] == 6'b000001 && Instruction[20:16] == 5'b00001)begin //bgez
            RegWriteSignal <= 0;
            //ALUSrc1Signal <= X;
            ALUSrc2Signal <= 0; 
            //RegDstSignal <= X; 
            MemReadSignal <= 0; 
            MemWriteSignal <= 0; 
            BranchSignal <= 1; 
            //MemToRegSignal <= X; 
            ALUOp <= 6'd12;
        end
        else if (Instruction[31:26] == 6'b000100)begin //beq
            RegWriteSignal <= 0;
            ALUSrc1Signal <= 0; 
            ALUSrc2Signal <= 0;
            //RegDstSignal <= X; 
            MemReadSignal <= 0; 
            MemWriteSignal <= 0; 
            BranchSignal <= 1; 
            //MemToRegSignal <= X; 
            ALUOp <= 6'd1;
        end
        else if (Instruction[31:26] == 6'b000101)begin //bne
            RegWriteSignal <= 0;
            ALUSrc1Signal <= 0; 
            ALUSrc2Signal <= 0;
            //RegDstSignal <= X; 
            MemReadSignal <= 0; 
            MemWriteSignal <= 0; 
            BranchSignal <= 1; 
            //MemToRegSignal <= X; 
            ALUOp <= 6'd13;
        end
        else if (Instruction[31:26] == 6'b000111)begin //bgtz
            RegWriteSignal <= 0;
            //ALUSrc1Signal <= X;
            ALUSrc2Signal <= 0; 
            //RegDstSignal <= X; 
            MemReadSignal <= 0; 
            MemWriteSignal <= 0; 
            BranchSignal <= 1; 
            //MemToRegSignal <= X; 
            ALUOp <= 6'd14;
        end
        else if (Instruction[31:26] == 6'b000110)begin //blez
            RegWriteSignal <= 0;
            //ALUSrc1Signal <= X; 
            ALUSrc2Signal <= 0;
            //RegDstSignal <= X; 
            MemReadSignal <= 0; 
            MemWriteSignal <= 0; 
            BranchSignal <= 1; 
            //MemToRegSignal <= X; 
            ALUOp <= 6'd15;
        end
        else if (Instruction[31:26] == 6'b000001 && Instruction[20:16] == 5'b00000)begin //bltz
            RegWriteSignal <= 0;
            //ALUSrc1Signal <= X;
            ALUSrc2Signal <= 0; 
            //RegDstSignal <= X; 
            MemReadSignal <= 0; 
            MemWriteSignal <= 0; 
            BranchSignal <= 1; 
            //MemToRegSignal <= X; 
            ALUOp <= 6'd16;
        end
        else if (Instruction[31:26] == 6'b000010)begin //j
            RegWriteSignal <= 0;
            //ALUSrc1Signal <= X;
            //ALUSrc2Signal <= X; 
            //RegDstSignal <= X; 
            MemReadSignal <= 0; 
            MemWriteSignal <= 0; 
            BranchSignal <= 1; 
            branchOrJump <= 1;
            //MemToRegSignal <= X; 
            ALUOp <= 6'd36;
        end
        else if (Instruction[31:26] == 6'b000000 && Instruction[5:0] == 6'b001000)begin //jr
            RegWriteSignal <= 0;
            //ALUSrc1Signal <= X;
            //ALUSrc2Signal <= 0; 
            //RegDstSignal <= X; 
            MemReadSignal <= 0; 
            MemWriteSignal <= 0; 
            BranchSignal <= 1; 
            //MemToRegSignal <= X; 
            branchOrJump <= 2;
            ALUOp <= 6'd36;
        end
        else if (Instruction[31:26] == 6'b000011)begin //jal
            RegWriteSignal <= 1;
            //ALUSrc1Signal <= X; 
            ALUSrc2Signal <= 0;
            RegDstSignal <= 2; 
            MemReadSignal <= 0; 
            MemWriteSignal <= 0; 
            BranchSignal <= 1; 
            branchOrJump <= 1;
            MemToRegSignal <= 1; 
            ALUOp <= 6'd35;
        end
        //End of branch instructions
        
        //Start of logical instructions
        else if(Instruction[31:26] == 6'b000000 && Instruction [5:0] == 6'b100100) begin //And
            RegWriteSignal <= 1;
            ALUSrc1Signal <= 0; 
            ALUSrc2Signal <= 0;
            RegDstSignal <= 1; 
            MemReadSignal <= 0; 
            MemWriteSignal <= 0; 
            BranchSignal <= 0; 
            MemToRegSignal <= 1; 
            ALUOp <= 6'd17;  
        end
        else if(Instruction[31:26] == 6'b001100) begin //Andi
            RegWriteSignal <= 1;
            ALUSrc1Signal <= 1;
            ALUSrc2Signal <= 0; 
            ALUSrc3Signal <= 1;
            RegDstSignal <= 0; 
            MemReadSignal <= 0; 
            MemWriteSignal <= 0; 
            BranchSignal <= 0; 
            MemToRegSignal <= 1; 
            ALUOp <= 6'd17;  
        end
        else if(Instruction[31:26] == 6'b000000 && Instruction [5:0] == 6'b100101) begin //Or
            RegWriteSignal <= 1;
            ALUSrc1Signal <= 0;
            ALUSrc2Signal <= 0; 
            RegDstSignal <= 1; 
            MemReadSignal <= 0; 
            MemWriteSignal <= 0; 
            BranchSignal <= 0; 
            MemToRegSignal <= 1; 
            ALUOp <= 6'd18;  
        end
        else if(Instruction[31:26] == 6'b000000 && Instruction [5:0] == 6'b100111) begin //Nor
            RegWriteSignal <= 1;
            ALUSrc1Signal <= 0;
            ALUSrc2Signal <= 0; 
            RegDstSignal <= 1; 
            MemReadSignal <= 0; 
            MemWriteSignal <= 0; 
            BranchSignal <= 0; 
            MemToRegSignal <= 1; 
            ALUOp <= 6'd19;  
        end
        else if(Instruction[31:26] == 6'b000000 && Instruction [5:0] == 6'b100110) begin //Xor
            RegWriteSignal <= 1;
            ALUSrc1Signal <= 0;
            ALUSrc2Signal <= 0; 
            RegDstSignal <= 1; 
            MemReadSignal <= 0; 
            MemWriteSignal <= 0; 
            BranchSignal <= 0; 
            MemToRegSignal <= 1; 
            ALUOp <= 6'd20;  
        end
        else if(Instruction[31:26] == 6'b001101) begin //Ori
            RegWriteSignal <= 1;
            ALUSrc1Signal <= 1;
            ALUSrc2Signal <= 0; 
            ALUSrc3Signal <= 1;
            RegDstSignal <= 0; 
            MemReadSignal <= 0; 
            MemWriteSignal <= 0; 
            BranchSignal <= 0; 
            MemToRegSignal <= 1; 
            ALUOp <= 6'd18;  
        end
        else if(Instruction[31:26] == 6'b001110) begin //Xori
            RegWriteSignal <= 1;
            ALUSrc1Signal <= 1;
            ALUSrc2Signal <= 0; 
            ALUSrc3Signal <= 1;
            RegDstSignal <= 0; 
            MemReadSignal <= 0; 
            MemWriteSignal <= 0; 
            BranchSignal <= 0; 
            MemToRegSignal <= 1; 
            ALUOp <= 6'd20;  
        end
        else if(Instruction[31:26] == 6'b011111 && Instruction[10:6] == 5'b11000 &&Instruction[5:0] == 6'b100000) begin //Seh
            RegWriteSignal <= 1;
            ALUSrc1Signal <= 0; 
            ALUSrc2Signal <= 0;
            RegDstSignal <= 1; 
            MemReadSignal <= 0; 
            MemWriteSignal <= 0; 
            BranchSignal <= 0; 
            MemToRegSignal <= 1; 
            ALUOp <= 6'd21;  
        end
        else if(Instruction[31:26] == 6'b000000 && Instruction[5:0] == 6'b000000) begin //Sll
            RegWriteSignal <= 1;
            ALUSrc1Signal <= 1;
            ALUSrc2Signal <= 1; 
            RegDstSignal <= 1; 
            MemReadSignal <= 0; 
            MemWriteSignal <= 0; 
            BranchSignal <= 0; 
            MemToRegSignal <= 1; 
            ALUOp <= 6'd22;  
        end
        else if(Instruction[31:26] == 6'b000000 && Instruction[21] == 0 && Instruction[5:0] == 6'b000010) begin //Srl
            RegWriteSignal <= 1;
            ALUSrc1Signal <= 1; 
            ALUSrc2Signal <= 1;
            RegDstSignal <= 1; 
            MemReadSignal <= 0; 
            MemWriteSignal <= 0; 
            BranchSignal <= 0; 
            MemToRegSignal <= 1; 
            ALUOp <= 6'd23;  
        end
        else if(Instruction[31:26] == 6'b000000 && Instruction[5:0] == 6'b000100) begin //Sllv
            RegWriteSignal <= 1;
            ALUSrc1Signal <= 0;
            ALUSrc2Signal <= 0; 
            RegDstSignal <= 1; 
            MemReadSignal <= 0; 
            MemWriteSignal <= 0; 
            BranchSignal <= 0; 
            MemToRegSignal <= 1; 
            ALUOp <= 6'd24;  
        end
        else if(Instruction[31:26] == 6'b000000 && Instruction[6] == 0 && Instruction[5:0] == 6'b000110) begin //Srlv
            RegWriteSignal <= 1;
            ALUSrc1Signal <= 0;
            ALUSrc2Signal <= 0; 
            RegDstSignal <= 1; 
            MemReadSignal <= 0; 
            MemWriteSignal <= 0; 
            BranchSignal <= 0; 
            MemToRegSignal <= 1; 
            ALUOp <= 6'd25;  
        end
        else if(Instruction[31:26] == 6'b000000 && Instruction[5:0] == 6'b101010) begin //Slt
            RegWriteSignal <= 1;
            ALUSrc1Signal <= 0; 
            ALUSrc2Signal <= 0;
            RegDstSignal <= 1; 
            MemReadSignal <= 0; 
            MemWriteSignal <= 0; 
            BranchSignal <= 0; 
            MemToRegSignal <= 1; 
            ALUOp <= 6'd26; 
        end
        else if(Instruction[31:26] == 6'b001010) begin //Slti
            RegWriteSignal <= 1;
            ALUSrc1Signal <= 1; 
            ALUSrc2Signal <= 0;
            RegDstSignal <= 0; 
            MemReadSignal <= 0; 
            MemWriteSignal <= 0; 
            BranchSignal <= 0; 
            MemToRegSignal <= 1; 
            ALUOp <= 6'd26; 
        end
        else if(Instruction[31:26] == 6'b000000 && Instruction[5:0] == 6'b001011) begin //movn
            RegWriteSignal <= 1;
            ALUSrc1Signal <= 0; 
            ALUSrc2Signal <= 0;
            RegDstSignal <= 1; 
            MemReadSignal <= 0; 
            MemWriteSignal <= 0; 
            BranchSignal <= 0; 
            MemToRegSignal <= 1; 
            ALUOp <= 6'd27; 
        end
        else if(Instruction[31:26] == 6'b000000 && Instruction[5:0] == 6'b001010) begin //movz
            RegWriteSignal <= 1;
            ALUSrc1Signal <= 0; 
            ALUSrc2Signal <= 0;
            RegDstSignal <= 1; 
            MemReadSignal <= 0; 
            MemWriteSignal <= 0; 
            BranchSignal <= 0; 
            MemToRegSignal <= 1; 
            ALUOp <= 6'd28; 
        end
        else if(Instruction[31:26] == 6'b000000 && Instruction[6] == 1 && Instruction[5:0] == 6'b000110) begin //rotrv
            RegWriteSignal <= 1;
            ALUSrc1Signal <= 0; 
            ALUSrc2Signal <= 0;
            RegDstSignal <= 1; 
            MemReadSignal <= 0; 
            MemWriteSignal <= 0; 
            BranchSignal <= 0; 
            MemToRegSignal <= 1; 
            ALUOp <= 6'd29; 
        end
        else if(Instruction[31:26] == 6'b000000 && Instruction[21] == 1 && Instruction[5:0] == 6'b000010) begin //rotr
            RegWriteSignal <= 1;
            ALUSrc1Signal <= 1; 
            ALUSrc2Signal <= 1;
            RegDstSignal <= 1; 
            MemReadSignal <= 0; 
            MemWriteSignal <= 0; 
            BranchSignal <= 0; 
            MemToRegSignal <= 1; 
            ALUOp <= 6'd30; 
        end
        else if(Instruction[31:26] == 6'b000000 && Instruction[5:0] == 6'b000011) begin //sra
            RegWriteSignal <= 1;
            ALUSrc1Signal <= 1; 
            ALUSrc2Signal <= 1;
            RegDstSignal <= 1; 
            MemReadSignal <= 0; 
            MemWriteSignal <= 0; 
            BranchSignal <= 0; 
            MemToRegSignal <= 1; 
            ALUOp <= 6'd31; 
        end
        else if(Instruction[31:26] == 6'b000000 && Instruction[5:0] == 6'b000111) begin //srav
            RegWriteSignal <= 1;
            ALUSrc1Signal <= 0;
            ALUSrc2Signal <= 0; 
            RegDstSignal <= 1; 
            MemReadSignal <= 0; 
            MemWriteSignal <= 0; 
            BranchSignal <= 0; 
            MemToRegSignal <= 1; 
            ALUOp <= 6'd32; 
        end
        else if(Instruction[31:26] == 6'b011111 && Instruction[10:6] == 5'b10000 &&Instruction[5:0] == 6'b100000) begin //seb
            RegWriteSignal <= 1;
            //ALUSrc1Signal <= 0; 
            ALUSrc2Signal <= 1;
            RegDstSignal <= 1; 
            MemReadSignal <= 0; 
            MemWriteSignal <= 0; 
            BranchSignal <= 0; 
            MemToRegSignal <= 1; 
            ALUOp <= 6'd33; 
        end
        else if(Instruction[31:26] == 6'b001011) begin //sltiu
           RegWriteSignal <= 1;
           ALUSrc1Signal <= 1; 
           ALUSrc2Signal <= 0;
           RegDstSignal <= 0; 
           MemReadSignal <= 0; 
           MemWriteSignal <= 0; 
           BranchSignal <= 0; 
           MemToRegSignal <= 1; 
           ALUOp <= 6'd34; 
       end
       else if(Instruction[31:26] == 6'b000000 && Instruction[5:0] == 6'b101011) begin //sltu
          RegWriteSignal <= 1;
          ALUSrc1Signal <= 0; 
          ALUSrc2Signal <= 0;
          RegDstSignal <= 1; 
          MemReadSignal <= 0; 
          MemWriteSignal <= 0; 
          BranchSignal <= 0; 
          MemToRegSignal <= 1; 
          ALUOp <= 6'd34; 
      end
    end
 endmodule