`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// ECE369 - Computer Architecture
// 
// Module - data_memory.v
// Description - 32-Bit wide data memory.
//
// INPUTS:-
//
// OUTPUTS:-
//
// FUNCTIONALITY: 
////////////////////////////////////////////////////////////////////////////////

module EXMEMRegister(Clk,MemToRegSignal,RegWriteSignal,MemWriteSignal,MemReadSignal,BranchSignal,
                    AdderResult, ALUResult, ReadData2, ZeroSignal,CheckRegWrite,RegDstAddress,
                    MemToRegSignalOut, RegWriteSignalOut,CheckRegWriteOut, MemWriteSignalOut,MemReadSignalOut, BranchSignalOut,
                    AdderResultOut, ALUResultOut, ReadData2Out, ZeroSignalOut,RegDstAddressOut); 

    input Clk;
    
    input MemToRegSignal,RegWriteSignal; //WB Signals
    output reg MemToRegSignalOut, RegWriteSignalOut; //WB Output signal
    
    input BranchSignal; //MEM Signals
    input [1:0] MemWriteSignal,MemReadSignal;
    output reg BranchSignalOut; //MEM Output Signals
    output reg [1:0] MemWriteSignalOut,MemReadSignalOut;
    
    
    input [31:0] AdderResult,ALUResult,ReadData2;   //Execution Results
    input CheckRegWrite;                            
    output reg [31:0] AdderResultOut,ALUResultOut, ReadData2Out;
    output reg CheckRegWriteOut;
    
    input ZeroSignal;
    output reg ZeroSignalOut;
    
    input [4:0] RegDstAddress;
    output reg [4:0] RegDstAddressOut;
    
    
    
    
    always @ (posedge Clk) begin
        //WB Signals
        MemToRegSignalOut <= MemToRegSignal;
        RegWriteSignalOut <= RegWriteSignal;
        
        //MEM Signals
        MemWriteSignalOut <= MemWriteSignal;
        MemReadSignalOut <= MemReadSignal;
        BranchSignalOut <= BranchSignal;
        
        //EX Signals
        AdderResultOut <= AdderResult;
        ALUResultOut <= ALUResult;
        ReadData2Out <= ReadData2;
        
        ZeroSignalOut <= ZeroSignal;
        CheckRegWriteOut <= CheckRegWrite;
        
        RegDstAddressOut <= RegDstAddress;
    end
endmodule