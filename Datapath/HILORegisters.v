`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 10/14/2018 02:37:03 PM
// Design Name: 
// Module Name: HILORegisters
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module HILORegister(Clk,WriteSignal,HI_in,HI_out,LO_in,LO_out,debug_HI,debug_LO);
    input Clk, WriteSignal;
    input [31:0] HI_in, LO_in;
    output reg [31:0] HI_out, LO_out;
    (* mark_debug = "true" *)reg [31:0] HI,LO;
    output wire [31:0] debug_HI, debug_LO;
    always @ (posedge Clk) begin
        if (WriteSignal == 1) begin
            HI <= HI_in;
            LO <= LO_in;
        end
    end
    always @ (negedge Clk) begin
        HI_out <= HI;
        LO_out <= LO;
    end
    assign debug_HI = HI;
    assign debug_LO = LO;
endmodule