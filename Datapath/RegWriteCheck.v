`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 10/10/2018 02:11:58 PM
// Design Name: 
// Module Name: RegWriteCheck
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module RegWriteCheck(A,B,Result);
    input A,B;
    output reg Result;
    
    
    always @(A,B) begin
        Result <= A ^ B;
    end
endmodule
