`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 10/11/2018 05:22:09 PM
// Design Name: 
// Module Name: Datapath_tb
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////

//Percent effort: Cordell Freeman %50
//			  Xuran Qiu %50
module Datapath_tb();
    reg Clk,Rst;
    
    Datapath dp(Clk,Rst);
    
    initial begin
        Clk <= 1'b0;
        Rst <= 1'b0;
        forever #5 Clk <= ~Clk;
    end
    
    initial begin
    #5  
    Rst <= 1;
    #5
    Rst <= 0;
    end
    
endmodule
