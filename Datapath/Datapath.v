`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 10/10/2018 11:36:08 PM
// Design Name: 
// Module Name: Datapath
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////

//Percent effort: Cordell Freeman %50
//			  Xuran Qiu %50
module Datapath(Clk,Reset);
    input Clk,Reset;
    
    //Debug
    (* mark_debug = "true" *)  wire [31:0] debug_reg_s0,debug_reg_s1,debug_reg_s2,debug_reg_s4,debug_reg_s5,debug_reg_writeData;
    (* mark_debug = "true" *)  wire [31:0] debug_reg_HI, debug_reg_LO,debug_reg_PCOut;
        
        //Stage 0 signals
        wire [31:0] PCAdderOut,PCSourceMuxOut,ProgramCounterOut,Instruction;
        
        //Pipeline register IF/ID signals
        wire [31:0] IF_ID_Reg_PCAdderOut,IF_ID_Reg_Instruction;
        
        //Stage 1 Signals
        wire [31:0] Immediate,zeroExtendOut,ReadData1,ReadData2;
        wire MemToReg,RegWrite,BranchSignal,ALUSrc1,ALUSrc2,ALUSrc3,writeHILO;
        wire [1:0] MemWrite,MemRead,branchOrJump,RegDst;
        wire [5:0] ALUOp;
        
        //Pipeline register ID/EX signals
        wire ID_EX_Reg_MemToReg,ID_EX_Reg_RegWrite,ID_EX_Reg_BranchSignal,ID_EX_Reg_ALUSrc1,ID_EX_Reg_ALUSrc2,ID_EX_Reg_ALUSrc3,ID_EX_Reg_writeHILO;
        wire [1:0] ID_EX_Reg_BranchOrJump,ID_EX_Reg_MemWrite,ID_EX_Reg_MemRead,ID_EX_Reg_RegDst;
        wire [4:0] ID_EX_Reg_Instruction20_16,ID_EX_Reg_Instruction15_11;
        wire [5:0] ID_EX_Reg_ALUOp;
        wire [25:0] ID_EX_Reg_Instruction25_0;
        wire [31:0] ID_EX_Reg_PCAdderOut,ID_EX_Reg_Immediate,ID_EX_Reg_ZeroExtend,ID_EX_Reg_ReadData1,ID_EX_Reg_ReadData2;
        wire [31:0] HI_out,HI_in,LO_out,LO_in;
                
        //Stage 2 Signals
        wire Zero,ChangeRegWrite;
        wire [4:0] RegDstMuxOut;
        wire [31:0] ShiftLeftOut,AdderOut,ALUSrc1MuxOut,ALUSrc2MuxOut,ALUSrc3MuxOut,ALUResult,branchOrJumpOut;
        
        //Pipeline register EX/MEM signals
        wire EX_MEM_Reg_MemToReg,EX_MEM_Reg_RegWrite,EX_MEM_Reg_BranchSignal,EX_MEM_Reg_ChangeRegWrite,EX_MEM_Reg_Zero;
        wire [1:0] EX_MEM_Reg_MemWrite,EX_MEM_Reg_MemRead;
        wire [4:0] EX_MEM_Reg_WriteRegister;
        wire [31:0] EX_MEM_Reg_AdderOut, EX_MEM_Reg_ALUResult, EX_MEM_Reg_ReadData2;       
        
        //Stage 3 Signals
        wire PCSource,NewRegWrite;
        wire [31:0] DataMemoryOut;
        
        //Pipeline register MEM/WB signals    
        wire MEM_WB_Reg_RegWrite,MEM_WB_Reg_MemToReg;
        wire [4:0] MEM_WB_Reg_WriteRegister;
        wire [31:0] MEM_WB_Reg_ALUResult,MEM_WB_Reg_DataFromMemory;
        
        //Stage 4 Signals
        wire [31:0] MemToRegMuxOut;
        
        //Start of Stage 0 Components
        Mux32Bit2To1 pcSourceMux(PCSourceMuxOut,PCAdderOut,EX_MEM_Reg_AdderOut,PCSource);   //module Mux32Bit2To1(out, inA, inB, sel);
        ProgramCounter programCounter(PCSourceMuxOut,ProgramCounterOut,Reset,Clk,debug_reg_PCOut);  //module ProgramCounter(Address, PCResult, Reset, Clk);
        PCAdder pcAdder(ProgramCounterOut,PCAdderOut);  //module PCAdder(PCResult, PCAddResult);
        InstructionMemory instructionMemory(ProgramCounterOut,Instruction); //module InstructionMemory(Address, Instruction);
        //End of Stage 0 Components
        
        //Pipeline register IF/ID
        IFIDRegister ifidRegister(PCAdderOut,Instruction,Clk,IF_ID_Reg_PCAdderOut,IF_ID_Reg_Instruction);    //module IFIDRegister(PCAdderResult, Instruction, Clk, PCAdderResultOut, InstructionOut);
        
        //Start of Stage 1 Components
        RegisterFile registerFile(IF_ID_Reg_Instruction[25:21],IF_ID_Reg_Instruction[20:16],MEM_WB_Reg_WriteRegister,MemToRegMuxOut,MEM_WB_Reg_RegWrite,Clk,ReadData1,ReadData2,debug_reg_s0,debug_reg_s1,debug_reg_s2,debug_reg_s4,debug_reg_s5,debug_reg_writeData);    //module RegisterFile(ReadRegister1, ReadRegister2, WriteRegister, WriteData, RegWrite, Clk, ReadData1, ReadData2);
        SignExtension signExtension(IF_ID_Reg_Instruction[15:0],Immediate); //module SignExtension(in, out);
        ZeroExtend zeroExtend(IF_ID_Reg_Instruction[15:0],zeroExtendOut);
        Controller controller(IF_ID_Reg_Instruction,MemToReg,RegWrite,MemWrite,MemRead,BranchSignal,ALUSrc1,ALUSrc2,ALUSrc3,RegDst,ALUOp,writeHILO,branchOrJump);    
                        //module Controller(Instruction,MemToRegSignal,RegWriteSignal,MemWriteSignal,MemReadSignal, BranchSignal,
                          //ALUSrc1Signal, ALUSrc2Signal, ALUSrc3Signal ,RegDstSignal, ALUOp,writeHILO);
        //End of Stage 1 Components
        
        //Pipeline register ID/EX
        IDEXRegister idexRegister(Clk,MemToReg,RegWrite,MemWrite,MemRead, BranchSignal,ALUSrc1,ALUSrc2, ALUSrc3,RegDst, ALUOp,IF_ID_Reg_PCAdderOut, ReadData1, ReadData2,Immediate,
                   IF_ID_Reg_Instruction[20:16],IF_ID_Reg_Instruction[15:11],writeHILO,zeroExtendOut,ID_EX_Reg_MemWrite,ID_EX_Reg_MemRead,ID_EX_Reg_BranchSignal,ID_EX_Reg_ALUSrc1,ID_EX_Reg_ALUSrc2,ID_EX_Reg_ALUSrc3,ID_EX_Reg_RegDst,ID_EX_Reg_ALUOp,
                   ID_EX_Reg_PCAdderOut, ID_EX_Reg_ReadData1, ID_EX_Reg_ReadData2, ID_EX_Reg_Immediate,ID_EX_Reg_Instruction20_16,ID_EX_Reg_Instruction15_11,ID_EX_Reg_MemToReg,ID_EX_Reg_RegWrite,ID_EX_Reg_writeHILO,ID_EX_Reg_ZeroExtend,branchOrJump,ID_EX_Reg_BranchOrJump,IF_ID_Reg_Instruction[25:0],ID_EX_Reg_Instruction25_0);
                        /*(Clk,MemToRegSignal,RegWriteSignal,MemWriteSignal,MemReadSignal, BranchSignal,ALUSrc1Signal,ALUSrc2Signal, RegDstSignal, ALUOp,PCAdderResult, ReadData1, ReadData2, SignExtendResult,
                            Instruction20_16,Instruction15_11,MemWriteSignalOut,MemReadSignalOut, BranchSignalOut,ALUSrc1SignalOut,ALUSrc2SignalOut, RegDstSignalOut,ALUOpOut,
                            PCAdderResultOut, ReadData1Out, ReadData2Out, SignExtendResultOut,Instruction20_16Out,Instruction15_11Out,MemToRegSignalOut,RegWriteSignalOut);*/
        
        
        //Start of Stage 2 Components
        ShiftLeft shiftLeft(ID_EX_Reg_Immediate,ShiftLeftOut);  //module ShiftLeft(SignExtend,ShiftOut);
        Adder adder(ID_EX_Reg_PCAdderOut,ShiftLeftOut,AdderOut);  //module Adder(PCAdderResult,ShiftResult,Result);
        Mux32Bit3To1 branchOrJumpMux(branchOrJumpOut,AdderOut,{ID_EX_Reg_PCAdderOut[31:28],ID_EX_Reg_Instruction25_0,2'b0},ID_EX_Reg_ReadData1,ID_EX_Reg_BranchOrJump);   //module Mux32Bit3To1(out, inA, inB, inC,sel);
        Mux32Bit2To1 ALUSrc1Mux(ALUSrc1MuxOut,ID_EX_Reg_ReadData2,ALUSrc3MuxOut,ID_EX_Reg_ALUSrc1);  //module Mux32Bit2To1(out, inA, inB, sel);
        Mux32Bit2To1 ALUSrc2Mux(ALUSrc2MuxOut,ID_EX_Reg_ReadData1,ID_EX_Reg_ReadData2,ID_EX_Reg_ALUSrc2);  //module Mux32Bit2To1(out, inA, inB, sel);
        Mux32Bit2To1 ALUSrc3Mux(ALUSrc3MuxOut,ID_EX_Reg_Immediate,ID_EX_Reg_ZeroExtend,ID_EX_Reg_ALUSrc3);  //module Mux32Bit2To1(out, inA, inB, sel);
        Mux5Bit3To1 RegDstMux(RegDstMuxOut,ID_EX_Reg_Instruction20_16,ID_EX_Reg_Instruction15_11,5'd31,ID_EX_Reg_RegDst);    //module Mux5Bit3To1(out, inA, inB,inC, sel);
        //Mux5Bit2To1 RegDstMux(RegDstMuxOut,ID_EX_Reg_Instruction20_16,ID_EX_Reg_Instruction15_11,ID_EX_Reg_RegDst);    //module Mux5Bit2To1(out, inA, inB, sel);    
        HILORegister hiloregister(Clk,ID_EX_Reg_writeHILO,HI_in,HI_out,LO_in,LO_out,debug_reg_HI, debug_reg_LO); //module HILORegister(Clk,WriteSignal,HI_in,HI_out,LO_in,LO_out,debug_HI,debug_LO);
        ALU32Bit alu(Clk,ID_EX_Reg_ALUOp,ALUSrc2MuxOut,ALUSrc1MuxOut,ID_EX_Reg_PCAdderOut,ALUResult,Zero,ChangeRegWrite,HI_out,LO_out,HI_in,LO_in); //module ALU32Bit(Clk,ALUControl, A, B, PcCounter,ALUResult, Zero, ChangeRegWrite,HI_in,LO_in,HI_out,LO_out);
        //End of Stage 2 Components  
        
        //Pipeline register EX/MEM
        EXMEMRegister exmemRegister(Clk, ID_EX_Reg_MemToReg, ID_EX_Reg_RegWrite, ID_EX_Reg_MemWrite, ID_EX_Reg_MemRead, ID_EX_Reg_BranchSignal,
                                branchOrJumpOut, ALUResult, ID_EX_Reg_ReadData2, Zero, ChangeRegWrite, RegDstMuxOut,
                                EX_MEM_Reg_MemToReg, EX_MEM_Reg_RegWrite, EX_MEM_Reg_ChangeRegWrite, EX_MEM_Reg_MemWrite, EX_MEM_Reg_MemRead, EX_MEM_Reg_BranchSignal,
                                EX_MEM_Reg_AdderOut, EX_MEM_Reg_ALUResult, EX_MEM_Reg_ReadData2, EX_MEM_Reg_Zero,EX_MEM_Reg_WriteRegister);
                        /*module EXMEMRegister(Clk,MemToRegSignal,RegWriteSignal,MemWriteSignal,MemReadSignal,BranchSignal,
                            AdderResult, ALUResult, ReadData2, ZeroSignal,CheckRegWrite,RegDstAddress,
                            MemToRegSignalOut, RegWriteSignalOut,CheckRegWriteOut, MemWriteSignalOut,MemReadSignalOut, BranchSignalOut,
                            AdderResultOut, ALUResultOut, ReadData2Out, ZeroSignalOut,RegDstAddressOut);*/
        
        //Start of Stage 3 Components
        Branch branch(EX_MEM_Reg_BranchSignal,EX_MEM_Reg_Zero,PCSource);    //module Branch(BranchSignal,ZeroValue,Result);
        RegWriteCheck regWriteCheck(EX_MEM_Reg_RegWrite,EX_MEM_Reg_ChangeRegWrite,NewRegWrite);  //module RegWriteCheck(A,B,Result);
        DataMemory dataMemory(EX_MEM_Reg_ALUResult,EX_MEM_Reg_ReadData2,Clk,EX_MEM_Reg_MemWrite,EX_MEM_Reg_MemRead,DataMemoryOut);    //module DataMemory(Address, WriteData, Clk, MemWrite, MemRead, ReadData);
        //End of Stage 3 Components
        
        //Pipeline register MEM/WB
        MEMWBRegister memwbRegister(Clk,EX_MEM_Reg_MemToReg,NewRegWrite,DataMemoryOut,EX_MEM_Reg_ALUResult,EX_MEM_Reg_WriteRegister,
                                    MEM_WB_Reg_MemToReg,MEM_WB_Reg_RegWrite,MEM_WB_Reg_DataFromMemory,MEM_WB_Reg_ALUResult,MEM_WB_Reg_WriteRegister);  
                        /*module MEMWBRegister(Clk, MemToRegSignal,RegWriteSignal,DataFromMem,ALUResult, RegDstAddress,  
                                                         MemToRegSignalOut,RegWriteSignalOut,DataFromMemOut,ALUResultOut,RegDstAddressOut);*/
        
        //Start of Stage 4 Components
        Mux32Bit2To1 memToRegMux(MemToRegMuxOut,MEM_WB_Reg_DataFromMemory,MEM_WB_Reg_ALUResult,MEM_WB_Reg_MemToReg);  //module Mux32Bit2To1(out, inA, inB, sel);
        //End of Stage 4 Components

endmodule
