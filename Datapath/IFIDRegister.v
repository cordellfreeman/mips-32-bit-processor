`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// ECE369 - Computer Architecture
// 
// Module - data_memory.v
// Description - 32-Bit wide data memory.
//
// INPUTS:-
//
// OUTPUTS:-
//
// FUNCTIONALITY: 
////////////////////////////////////////////////////////////////////////////////

module IFIDRegister(PCAdderResult, Instruction, Clk, PCAdderResultOut, InstructionOut); 

    input Clk;
    input [31:0] PCAdderResult, Instruction;
    
    output reg[31:0] PCAdderResultOut, InstructionOut;
    
    always @ (posedge Clk) begin
        PCAdderResultOut <= PCAdderResult;
        InstructionOut <= Instruction;
    end
endmodule