`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 10/03/2018 05:30:40 PM
// Design Name: 
// Module Name: Branch
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module Branch(BranchSignal,ZeroValue,Result);
    input BranchSignal,ZeroValue;
    output reg Result;
    always @ (BranchSignal,ZeroValue) begin
        Result <= BranchSignal & ZeroValue;
    end
endmodule
